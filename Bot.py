import random
import config
import telebot

from SQLighter import SQLighter

bot = telebot.TeleBot(config.token)


def create_assignments_pack(subj_name, number_of_assignments):
    # Подключаемся к БД
    db_worker = SQLighter(config.database_name)
    all_assignments = [[] for i in range(0, number_of_assignments)]
    for row in db_worker.select_all():
        if row[1] == subj_name:  # row[1] - subj name, row [4] - assignment number
            all_assignments[row[4] - 1].append(
                row[3])  # собираем (групируем) задания по их номеру
    # Отсоединяемся от БД
    assignments = []
    for assignment in all_assignments:
        assignments.append(assignment[random.randint(0, 30) % len(assignment)])
    db_worker.close()
    return assignments


@bot.message_handler(commands=['math'])
def math(message):
    assignments = create_assignments_pack("MATH", config.math_number_of_assignments)
    for element in assignments:
        bot.send_photo(message.chat.id, element)


@bot.message_handler(content_types=["text"])
def answer_for_any_massage(message):
    bot.send_message(message.chat.id, "для получения варианта напишите одну из команд: \n /math - математика база")


if __name__ == '__main__':
    bot.infinity_polling()
